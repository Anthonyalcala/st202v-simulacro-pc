/*
 * .hpp
 *
 *  Created on: 10 abr. 2018
 *      Author: PCA
 */

#ifndef UTILITARIOS_UTILITARIOS_HPP_
#define UTILITARIOS_UTILITARIOS_HPP_

#include<iostream>
using namespace std;

void implista(int lista[], int cant);
void impmat(char lista[][], int m, int n );
void revertir(int vector[], int cant);
void revertir2(char vector[], int cant);
void convertir(int vector[], int cant);
void ordenamientoBurbuja(int numeros[], int cantidadElementos);


#endif /* UTILITARIOS_UTILITARIOS_HPP_ */
