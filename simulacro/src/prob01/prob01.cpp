/*
 * prob01.cpp
 *
 *  Created on: 10 abr. 2018
 *      Author: PCA
 */
#include <iostream>
#include "../utilitarios/utilitarios.hpp"
using namespace std;

double calcularMedia(int datos[], int cant)
{
	int suma=0;
	float resultado=0;
	for(int i=0;i<cant;i++){
		suma+=datos[i];
	}
	resultado = (float(suma))/cant;
	return resultado;
}

double calcularMediana(int datos[], int cantidadElementos){
	ordenamientoBurbuja(datos, cantidadElementos);
	if(cantidadElementos%2!=0){
		return datos[(cantidadElementos-1)/2];
	}else{
		return 1.0*(datos[cantidadElementos/2]+datos[cantidadElementos/2-1])/2;
	}

}


void prob01()
{
	cout<<"Solucion prob01"<<endl;
	int cantidadElementos = 6;
	int casoPrueba[] = {7,5,1,22,14,8};
	double media = calcularMedia(casoPrueba, cantidadElementos);
	double mediana = calcularMediana(casoPrueba,cantidadElementos);
	cout<<"La media es:"<<media<<endl;
	cout<<"La mediana es:"<<mediana<<endl;

	if(media>mediana){
		cout<<"La media es mayor que la mediana"<<endl;
	}else if(mediana<media){
		cout<<"La mediana es mayor que la mediana"<<endl;
	}else{
		cout<<"Los valores de media y mediana son iguales"<<endl;
	}

}



