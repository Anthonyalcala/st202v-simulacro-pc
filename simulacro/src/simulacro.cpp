//============================================================================
// Name        : simulacro.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "prob01/prob01.hpp"
#include "prob02/prob02.hpp"
#include "prob03/prob03.hpp"
#include "prob04/prob04.hpp"
#include "prob05/prob05.hpp"
//#include "prob06/prob06.hpp"
//#include "prob07/prob07.hpp"


using namespace std;

int main() {
	prob01();
	prob02();
	prob03();
	prob04();
	prob05();
	return 0;
}

